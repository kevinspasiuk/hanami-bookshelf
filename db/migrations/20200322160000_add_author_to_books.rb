Hanami::Model.migration do
  change do
    alter_table :books do
      add_column :author, String
    end
  end
end