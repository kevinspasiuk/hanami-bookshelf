RSpec.describe AuthorRepository, type: :repository do
  it 'returns the same author that I saved' do
    author_name = "Jane Austen"

    jane = AuthorRepository.new.create(name: author_name)
    retrieved_author = AuthorRepository.new.find(jane.id)

    expect(retrieved_author.name).to eq(author_name)
  end
end
