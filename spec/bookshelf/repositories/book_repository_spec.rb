RSpec.describe BookRepository, type: :repository do
  it 'returns the same book that I saved' do
    book_title = "Mansfield's Park"

    book = BookRepository.new.create(title: book_title)
    retrieved_book = BookRepository.new.find(book.id)

    expect(retrieved_book.title).to eq(book_title)
  end

  it 'can save a Book already created' do
    book_title = "Sense and Sensibility"
    book = Book.new(title: book_title)
    
    saved_book = BookRepository.new.create(book)
    retrieved_book = BookRepository.new.find(saved_book.id)
  
    expect(retrieved_book).to eq(saved_book)
  end

  it 'retrieves de relationship between Book and Author' do
    book_title = "Pride and Prejudice"
    jane = AuthorRepository.new.create(name: "Jane Austen")
    
    book = BookRepository.new.create(title: book_title, author_id: jane.id)
    retrieved_book = BookRepository.new.find_with_author(book.id)

    expect(retrieved_book.author).to eq(jane)
  end

  # I would like to do something like this, but fails

=begin
  it 'can save the book with the relationship' do
    jane = AuthorRepository.new.create(name: "Jane Austen")
    book_title = "Pride and Prejudice"
    book = Book.new(title: book_title, author: jane)

    saved_book = BookRepository.new.create(book)
    retrieved_book = BookRepository.new.find_with_author(saved_book.id)

    expect(retrieved_book.author).to eq(jane)
  end
end
=end