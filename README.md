# Bookshelf

Welcome to your new Hanami project!

## Setup

How to run tests:

```
% HANAMI_ENV=test bundle exec hanami db prepare &  bundle exec rake
```

How to run the development console:

```
% bundle exec hanami console
```

How to run the development server:

```
% bundle exec hanami server
```

How to prepare (create and migrate) DB for `development` and `test` environments:

```
% bundle exec hanami db prepare

% HANAMI_ENV=test bundle exec hanami db prepare
```

Explore Hanami [guides](https://guides.hanamirb.org/), [API docs](http://docs.hanamirb.org/1.3.3/), or jump in [chat](http://chat.hanamirb.org) for help. Enjoy! 🌸


Run console with development db:

 HANAMI_ENV=development bundle exec hanami console

  BookRepository.new.all
  BookRepositor.new.crete(title: 'My Book')


  Run test with enviroment 
  HANAMI_ENV=development bundle exec rake
  
# Warnings

- Frozen entities, only on creation you can set attributes.
- attributes are taken from sql schema, not from entity.
- 'bundle exec hanami db prepare' rebuilds everything, run 'bundle exec hanami db migrate' to only migrate.
- migration with utc.
- respository.new.create(instance)? yes! if id already exists, it fails.
- updates works  only with id. 

class with attr defined by schema:
Book.new(title:'pride and prejudice',author:'jane austen', published_date:'1813')

# to do

- chek if you can update providing the instance
- run rake with env= test
- class with methods.

Asociete an author:
jane = AuthorRepository.new.create(name:'Jane Austen')
book = BookRepository.new.create(title:'pride and prejudice', author_id: jane.id)

BookRepository.new.find(book.id)

 => #<Book:0x00005598ba2c1750 @attributes={:id=>5, :title=>"pride and prejudice", :created_at=>2020-03-22 20:39:58 UTC, :updated_at=>2020-03-22 20:39:58 UTC, :author=>nil, :author_id=>1}>

 book = BookRepository.new.find_with_author(book.id)

 #<Book:0x00005598baf29c40 @attributes={:id=>5, :title=>"pride and prejudice", :created_at=>2020-03-22 20:39:58 UTC, :updated_at=>2020-03-22 20:39:58 UTC, :author=>#<Author:0x00005598baf290d8 @attributes={:id=>1, :name=>"Jane Austen", :created_at=>2020-03-22 20:28:36 UTC, :updated_at=>2020-03-22 20:28:36 UTC}>, :author_id=>1}>

 book.author.name
 => "Jane Austen"